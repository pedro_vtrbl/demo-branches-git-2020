package com.almasb.calc;

public class Model {

    public double calculate(double number1, double number2, String operator) {
        switch (operator) {
            case "+":
                return number1 + number2;
            case "-":
                return number1 - number2;
            case "*":
                return number1 * number2;
            case "/":
                if (number2 == 0) {
                    throw new IllegalArgumentException("Divsion par 0 pas possible");
                }
                return number1 / number2;
        }

        System.out.println("L'opérateur '" + operator +"' est inconnu !");
        return 0;
    }
}
